module.exports = {
  siteMetadata: {
    title: "AV-SpaceX | An Unofficial SpaceX DB",
    author: "Aswin Varghese",
    description: "A project bootstrapped using Gatsby.js for SpaceX Data using the SpaceX API"
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'AV-SpaceX',
        short_name: 'AV-SpaceX',
        start_url: '/',
        background_color: '#000',
        theme_color: '#000',
        display: 'minimal-ui',
        icon: 'src/images/AVlogo.png',
      },
    },
      {
          resolve: `gatsby-plugin-google-analytics`,
          options: {
              trackingId: "UA-107950553-4",
              head: true,
          },
      },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline'
  ],
}
