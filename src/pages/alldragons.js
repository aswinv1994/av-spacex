import React, { Component }  from 'react';
import axios from 'axios';
import moment from 'moment'


import { Link } from 'gatsby'


import Layout from '../components/layout'

let headstyle = {
    textAlign: 'center',
    width: '100%'
};

// import pic01 from '../images/pic01.jpg'

class alldragons extends Component {

    constructor(props) {
        super(props);

        this.state = {
            alldragonsData: []
        };
    }

    componentDidMount(){
        let alldragonsAPI = 'https://api.spacexdata.com/v3/dragons';
        axios.get(alldragonsAPI).then(res=> {
            console.log(res.data);
            this.setState({alldragonsData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major" style={headstyle}>All Dragons | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.alldragonsData && this.state.alldragonsData.map(dragon => {
                    return(
                        <div className="image main">
                            <h2>{dragon.name}</h2>
                            <h4>Type : {dragon.type} &emsp;&emsp;&emsp; Crew Capacity : {dragon.crew_capacity}</h4>
                            <h4>First flight : {moment(dragon.first_flight).calendar()}</h4>
                            <h4>Thrusters :
                                {dragon.thrusters.map(function(thruster , key){
                                    return(
                                        <h5 className="payloads-details" key={key}>({key+1}){thruster.type}<br/>
                                            Thrust : {thruster.thrust.kN}KN<br/>
                                            Amount : {thruster.amount} &#8594;
                                            Pods : {thruster.pods}<br/>
                                            Fuel : {thruster.fuel_1} & {thruster.fuel_2}<br/>
                                        </h5>
                                    )
                                })}
                            </h4>
                            <h4><u>Payloads</u><br/>
                                <h5>Launch &#8594; Mass : {dragon.launch_payload_mass.kg}kg&emsp;&emsp;&&emsp;&emsp;Vol : {dragon.launch_payload_vol.cubic_meters}m<sup>3</sup><br/>
                                    Return &#8594; Mass : {dragon.return_payload_mass.kg}kg&emsp;&emsp;&&emsp;&emsp;Vol : {dragon.return_payload_vol.cubic_meters}m<sup>3</sup><br/>
                                </h5>
                            </h4>
                            <h4>Dimensions : <div className="payloads-details">Height : {dragon.height_w_trunk.meters}M<br/>Diameter : {dragon.diameter.meters}M</div></h4>
                            {dragon.active === true ? <h4>Status : Active</h4>:<h4>Status : Inactive</h4>}
                            <h4>Details : {dragon.description}</h4>

                        </div>
                    )
                })}
            </Layout>
        )
    }
}

export default alldragons