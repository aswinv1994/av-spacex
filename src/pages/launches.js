import React, { Component }  from 'react';
import { Link } from 'gatsby'

import Pastlaunches from './pastlaunches'
import Futurelaunches from './futurelaunches'


class Launchpads extends Component {

    render(){
        return(
            <div>
                    <h1 className="major">Launches</h1>
                    <h2>10 Upcoming Launches<br/><Link to='/allfuturelaunches'>Show all</Link></h2>
                        <Futurelaunches/>
                    <h2>Past 10 Launches<br/><Link to='/allpastlaunches'>Show all</Link></h2>
                        <Pastlaunches/>
            </div>
        )
    }
}

export default Launchpads