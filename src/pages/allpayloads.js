import React, { Component }  from 'react';
import axios from 'axios';

import { Link } from 'gatsby'


import Layout from '../components/layout'

let headstyle = {
    textAlign: 'center',
    width: '100%'
};

// import pic01 from '../images/pic01.jpg'

class allpayloads extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allpayloadsData: []
        };
    }

    componentDidMount(){
        let allpayloadsAPI = 'https://api.spacexdata.com/v3/payloads';
        axios.get(allpayloadsAPI).then(res=> {
            console.log(res.data);
            this.setState({allpayloadsData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major" style={headstyle}>All Payloads | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.allpayloadsData && this.state.allpayloadsData.map(payload => {
                    return(
                        <div className="image main">
                            <h2>{payload.payload_id}</h2>
                            <h4>Nationality : {payload.nationality}&#8594; Manufactured by {payload.manufacturer} <br/>
                                Payload type : {payload.payload_type}&#8594; Mass : {payload.payload_mass_kg? payload.payload_mass_kg+"kg": "unknown"} </h4>
                            <h4>Orbit : {payload.orbit}
                                <div className="payloads-details">
                                    Orbit ref system : {payload.orbit_params.reference_system}<br/>Orbit regime : {payload.orbit_params.regime}
                                </div>
                            </h4>
                            <h4>Customers :
                                {payload.customers !== '' ?
                                    payload.customers.map(function(customer , key){
                                        return(
                                            <h5 className="payloads-details" key={key}>({key+1}){customer}</h5>
                                        )
                                    }): <h5>To be announced</h5>}
                            </h4>
                            <h4>Status : {payload.reused === false ? "Not reused" : "Reused"}</h4>
                        </div>
                    )
                })}
            </Layout>
        )
    }
}

export default allpayloads