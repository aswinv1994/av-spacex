import React, { Component }  from 'react';
import axios from 'axios';
import moment from 'moment'


import { Link } from 'gatsby'


import Layout from '../components/layout'

let headstyle = {
    textAlign: 'center',
    width: '100%'
};

// import pic01 from '../images/pic01.jpg'

class allcapsules extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allcapsulesData: []
        };
    }

    componentDidMount(){
        let allcapsulesAPI = 'https://api.spacexdata.com/v3/capsules';
        axios.get(allcapsulesAPI).then(res=> {
            console.log(res.data);
            this.setState({allcapsulesData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major" style={headstyle}>All Capsules | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.allcapsulesData && this.state.allcapsulesData.map(capsule => {
                    return(
                        <div className="image main">
                            <h2>{capsule.capsule_serial}</h2>
                            <h4>Type : {capsule.type} &emsp;&emsp;&emsp;Landings : {capsule.landings} &#8594; Reuse Count : {capsule.reuse_count}</h4>
                            <h4>Orginal Launch : {capsule.original_launch ? moment(capsule.original_launch).format('MMMM Do YYYY, h:mm:ss a'): "To be announced"}</h4>
                            <h4>Missions :
                                {capsule.missions !== '' ?
                                    capsule.missions.map(function(mission , key){
                                    return(
                                        <h5 className="payloads-details" key={key}>({key+1}){mission.name} &#8594; Flight : {mission.flight}</h5>
                                    )
                                }): <h5>To be announced</h5>}
                            </h4>
                            <h4>Status : {capsule.status}</h4>
                            <h4>Details : {capsule.details ? capsule.details: "Unavailable" }</h4>

                        </div>
                    )
                })}
            </Layout>
        )
    }
}

export default allcapsules