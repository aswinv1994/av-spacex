import React, { Component }  from 'react';
import axios from 'axios';
import moment from 'moment'


import { Link } from 'gatsby'


import Layout from '../components/layout'

let headstyle = {
    textAlign: 'center',
    width: '100%'
};

// import pic01 from '../images/pic01.jpg'

class allcores extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allcoresData: []
        };
    }

    componentDidMount(){
        let allcoresAPI = 'https://api.spacexdata.com/v3/cores';
        axios.get(allcoresAPI).then(res=> {
            console.log(res.data);
            this.setState({allcoresData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major" style={headstyle}>All Cores | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.allcoresData && this.state.allcoresData.map(core => {
                    return(
                        <div className="image main">
                            <h2>{core.core_serial}</h2>
                            <h4>Blocks : {core.block === null ? 0 : core.block }<br/> Water Landings : {core.water_landing ? "Yes": "No"} &#8594; Reuse Count : {core.reuse_count}</h4>
                            <h4>Orginal Launch : {core.original_launch ? moment(core.original_launch).format('MMMM Do YYYY, h:mm:ss a'): "To be announced"}</h4>
                            <h4>Missions :
                                {core.missions !== '' ?
                                    core.missions.map(function(mission , key){
                                        return(
                                            <h5 className="payloads-details" key={key}>({key+1}){mission.name} &#8594; Flight : {mission.flight}</h5>
                                        )
                                    }): <h5>To be announced</h5>}
                            </h4>
                            <h4>Status : {core.status}</h4>
                            <h4>Details : {core.details ? core.details: "Unavailable" }</h4>

                        </div>
                    )
                })}
            </Layout>
        )
    }
}

export default allcores