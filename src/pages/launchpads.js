import React, { Component }  from 'react';
import axios from 'axios';
import { Link } from 'gatsby'

import Layout from '../components/layout'

class Launchpads extends Component {

    constructor(props) {
        super(props);

        this.state = {
            launchpadData: []
        };
    }

    componentDidMount(){
        let launchpadAPI = 'https://api.spacexdata.com/v3/launchpads';
        axios.get(launchpadAPI).then(res=> {
            console.log(res.data);
            this.setState({launchpadData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major"><Link to="/" style={{borderBottom:'none'}}>&#8678;</Link> Launchpads</h1>
                    <h3>Active Launchpads </h3>
                {this.state.launchpadData && this.state.launchpadData.map(function(lpads, key) {
                    if(lpads.status==="active"){
                        return(
                            <div key={key} className="image main">
                            <h4>{lpads.site_name_long}</h4>
                                <h5>Location : {lpads.location.name}, {lpads.location.region}</h5>
                                <h5>{lpads.details}</h5>
                                <h5>Attempted Lauches : {lpads.attempted_launches}<br/>Successfull Launches : {lpads.successful_launches}</h5>
                                <h5>Vehicles used :
                                {lpads.vehicles_launched.map(vehicle=>{
                                    return(
                                        <li>{vehicle}</li>
                                    )
                                })}
                                </h5>
                                <h5><a href={lpads.wikipedia}>More Details</a></h5>
                            </div>
                        )
                    }
                })}
                    <h3>Under Construction Launchpads</h3>
                        {this.state.launchpadData && this.state.launchpadData.map(function(lpads, key) {
                            if(lpads.status==="under construction"){
                                return(
                                    <div key={key} className="image main">
                                        <h4>{lpads.site_name_long}</h4>
                                        <h5>Location : {lpads.location.name}, {lpads.location.region}</h5>
                                        <h5>{lpads.details}</h5>
                                        <h5>Attempted Lauches : {lpads.attempted_launches}<br/>Successfull Launches : {lpads.successful_launches}</h5>
                                        <h5>Vehicles used :
                                            {lpads.vehicles_launched.map(vehicle=>{
                                                return(
                                                    <li>{vehicle}</li>
                                                )
                                            })}
                                        </h5>
                                        <h5><a href={lpads.wikipedia}>More Details</a></h5>
                                    </div>
                                )
                            }
                        })}

                        <h3>Closed Launchpads</h3>
                        {this.state.launchpadData && this.state.launchpadData.map(function(lpads, key) {
                            if(lpads.status==="retired"){
                                return(
                                    <div key={key} className="image main">
                                        <h4>{lpads.site_name_long}</h4>
                                        <h5>Location : {lpads.location.name}, {lpads.location.region}</h5>
                                        <h5>{lpads.details}</h5>
                                        <h5>Attempted Lauches : {lpads.attempted_launches}<br/>Successfull Launches : {lpads.successful_launches}</h5>
                                        <h5>Vehicles used :
                                            {lpads.vehicles_launched.map(vehicle=>{
                                                return(
                                                    <li>{vehicle}</li>
                                                )
                                            })}
                                        </h5>
                                        <h5><a href={lpads.wikipedia}>More Details</a></h5>
                                    </div>
                                )
                            }
                        })}
            </Layout>
        )
    }
}

export default Launchpads