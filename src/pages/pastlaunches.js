import React, { Component }  from 'react';
import axios from 'axios';
import moment from 'moment'



// import pic01 from '../images/pic01.jpg'

class pastLaunches extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pastlaunchData: []
        };
    }

    componentDidMount(){
        let pastlaunchAPI = 'https://api.spacexdata.com/v3/launches/past?limit=10&order=desc';
        axios.get(pastlaunchAPI).then(res=> {
            console.log(res.data);
            this.setState({pastlaunchData: res.data})
        });
    }


    render(){
        return(
            <div>

                    {this.state.pastlaunchData && this.state.pastlaunchData.map(function(launch, key) {
                        if(launch.launch_success === true){
                            return(
                                <div className="image main" key={key}>
                                    <h3>{launch.mission_name}</h3>
                                    <h4>Launched on {moment(launch.launch_date_utc).format('MMMM Do YYYY, h:mm:ss a')}<br/>Launch site : {launch.launch_site.site_name_long}</h4>
                                    <h4>Launch : Success, {launch.details}</h4>
                                </div>
                            )
                        }

                        else {
                            return(
                                <div className="image main" key={key}>
                                    <h3>{launch.mission_name}</h3>
                                    <h4>Launched on {moment(launch.launch_date_utc).format('MMMM Do YYYY, h:mm:ss a')}<br/>Launch site : {launch.launch_site.site_name_long}</h4>
                                    <h4>Launch : Failed, {launch.details}</h4>
                                </div>
                            )
                        }

                    })}
            </div>
        )
    }
}

export default pastLaunches