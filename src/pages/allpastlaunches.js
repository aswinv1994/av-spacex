import React, { Component }  from 'react';
import axios from 'axios';
import { Link } from 'gatsby'
import {CircleArrow as ScrollUpButton} from "react-scroll-up-button";



import Layout from '../components/layout'
import moment from "moment/moment";

let headstyle = {
    textAlign: 'center',
    width: '100%'
};
// import pic01 from '../images/pic01.jpg'

class pastLaunches extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pastlaunchData: []
        };
    }

    componentDidMount(){
        let pastlaunchAPI = 'https://api.spacexdata.com/v3/launches/past?order=desc';
        axios.get(pastlaunchAPI).then(res=> {
            console.log(res.data);
            this.setState({pastlaunchData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                    <h1 className="major" style={headstyle}>Past Launches | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.pastlaunchData && this.state.pastlaunchData.map(launch => {
                    if(launch.launch_success === true){
                        return(
                            <div className="image main">
                                <h2>{launch.mission_name}</h2>
                                <h4>Flight number : #{launch.flight_number}</h4>
                                <h4>Launched on {moment(launch.launch_date_utc).format('MMMM Do YYYY, h:mm:ss a')}<br/>Launch site : {launch.launch_site.site_name_long}</h4>
                                <h4>Rocket Name : {launch.rocket.rocket_name} [Type -- {launch.rocket.rocket_type}]</h4>
                                <h4><u>First stage :: cores</u><br/>
                                    {launch.rocket.first_stage.cores.map(function(core, key){
                                        return(
                                            <h5 key={key}>({key+1}){core.core_serial}<br/>
                                                <div className="payloads-details">
                                                    Landing Vehicle : {core.landing_vehicle}<br/>
                                                    Landing type : {core.landing_type}
                                                    <br/>
                                                    Reused : {core.reused === true ? "Yes": "No"}
                                                </div>
                                            </h5>
                                        )
                                    })}
                                </h4>
                                <h4><u>Second stage :: Payloads</u><br/>
                                    {launch.rocket.second_stage.payloads.map(function(payload , key){
                                        return(
                                            <h5 key={key}>({key+1}){payload.payload_id}<br/>
                                                <div className="payloads-details">
                                                    Reused : {payload.reused === true ? "Yes": "No"}<br/>
                                                    Customers : {payload.customers.map(cust =>{
                                                    return(
                                                        <li>{cust}</li>
                                                    )
                                                })}
                                                    <br/>Nationality : {payload.nationality} <br/>Manufactured by {payload.manufacturer ? payload.manufacturer: "Unknown"}<br/>
                                                    payload type : {payload.payload_type}
                                                </div>
                                            </h5>
                                        )
                                    })}
                                </h4>
                                {launch.details ? <h4>Launch : Successfull<br/>Launch Details : {launch.details}</h4>: <h4>Launch : Successfull<br/>Launch Details : nill</h4>}
                            </div>
                        )
                    }

                    else {
                        return(
                            <div className="image main">
                                <h2>{launch.mission_name}</h2>
                                <h4>Flight number : #{launch.flight_number}</h4>
                                <h4>Launched on {moment(launch.launch_date_utc).format('MMMM Do YYYY, h:mm:ss a')}<br/>Launch site : {launch.launch_site.site_name_long}</h4>
                                <h4>Rocket Name : {launch.rocket.rocket_name} [Type -- {launch.rocket.rocket_type}]</h4>
                                <h4><u>First stage :: cores</u><br/>
                                    {launch.rocket.first_stage.cores.map(function(core, key){
                                        return(
                                            <h5 key={key}>({key+1}){core.core_serial}<br/>
                                                <div className="payloads-details">
                                                    Landing Vehicle : {core.landing_vehicle}<br/>
                                                    Landing type : {core.landing_type}
                                                    <br/>
                                                    Reused : {core.reused === true ? "Yes": "No"}
                                                </div>
                                            </h5>
                                        )
                                    })}
                                </h4>
                                <h4><u>Second stage :: Payloads</u><br/>
                                    {launch.rocket.second_stage.payloads.map(function(payload , key){
                                        return(
                                            <h5 key={key}>({key+1}){payload.payload_id}<br/>
                                                <div className="payloads-details">
                                                    Reused : {payload.reused === true ? "Yes": "No"}<br/>
                                                    Customers : {payload.customers.map(cust =>{
                                                    return(
                                                        <li>{cust}</li>
                                                    )
                                                })}
                                                    <br/>Nationality : {payload.nationality} <br/>Manufactured by {payload.manufacturer ? payload.manufacturer: "Unknown"}<br/>
                                                    payload type : {payload.payload_type}
                                                </div>
                                            </h5>
                                        )
                                    })}
                                </h4>
                                {launch.details ? <h4>Launch : Failed<br/>Launch Details : {launch.details}</h4>: <h4>Launch : Failed<br/>Launch Details : nill</h4>}
                            </div>
                        )
                    }

                })}
                <ScrollUpButton
                    StopPosition={0}
                    ShowAtPosition={150}
                    EasingType='easeOutCubic'
                    AnimationDuration={500}
                    ContainerClassName='ScrollUpButton__Container'
                    TransitionClassName='ScrollUpButton__Toggled'
                    style={{backgroundColor: "transparent", border: "5px solid #4a4a4a"}}
                    ToggledStyle={{}}
                />
            </Layout>
        )
    }
}

export default pastLaunches