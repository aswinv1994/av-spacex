import React, { Component }  from 'react';
import axios from 'axios';
import moment from 'moment'



// import pic01 from '../images/pic01.jpg'

class futureLaunches extends Component {

    constructor(props) {
        super(props);

        this.state = {
            futurelaunchData: []
        };
    }

    componentDidMount(){
        let futurelaunchAPI = 'https://api.spacexdata.com/v3/launches/upcoming?limit=10&order=asc';
        axios.get(futurelaunchAPI).then(res=> {
            console.log(res.data);
            this.setState({futurelaunchData: res.data})
        });
    }


    render(){
        return(
            <div>

                {this.state.futurelaunchData && this.state.futurelaunchData.map(function(launch, key) {
                        return(
                            <div className="image main" key={key}>
                                <h3>{launch.mission_name}</h3>
                                <h4>Flight number : #{launch.flight_number}</h4>
                                <h4>Launching on {moment(launch.launch_date_utc).format('MMMM Do YYYY, h:mm:ss a')}<br/>Launch site : {launch.launch_site.site_name_long}</h4>
                                <h4>Rocket Name : {launch.rocket.rocket_name} of Type {launch.rocket.rocket_type}</h4>
                            </div>
                        )
                })}
            </div>
        )
    }
}

export default futureLaunches