import React, { Component }  from 'react';
import { Link } from 'gatsby'


import Layout from '../components/layout'

class page2 extends Component {
    render(){
        return(
            <Layout>
                <h1 className="major"><Link to="/" style={{borderBottom:'none'}}>&#8678;</Link> ERROR PAGE</h1>
            </Layout>
        )
    }
}

export default page2
