import React, { Component }  from 'react';
import axios from 'axios';

import { Link } from 'gatsby'


import Layout from '../components/layout'

let headstyle = {
    textAlign: 'center',
    width: '100%'
};

// import pic01 from '../images/pic01.jpg'

class allrockets extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allrocketsData: []
        };
    }

    componentDidMount(){
        let allrocketsAPI = 'https://api.spacexdata.com/v3/rockets';
        axios.get(allrocketsAPI).then(res=> {
            console.log(res.data);
            this.setState({allrocketsData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major" style={headstyle}>All Rockets | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.allrocketsData && this.state.allrocketsData.map(rocket => {
                    return(
                        <div className="image main">
                            <h2>{rocket.rocket_name}</h2>
                            <h4>Stages : {rocket.stages} &emsp;&emsp; Boosters : {rocket.boosters}<br/>Cost/Launch : ${rocket.cost_per_launch}
                                <br/>Success : {rocket.success_rate_pct}%</h4>
                            <h4>Dimensions : <div className="payloads-details">Height : {rocket.height.meters}M<br/>Diameter : {rocket.diameter.meters}M<br/>Weight : {rocket.mass.kg}Kg</div></h4>
                            <h4>Engine : <div className="payloads-details">Type : {rocket.engines.type}, V-{rocket.engines.version === ""?"1":rocket.engines.version}<br/>
                                Propellants : {rocket.engines.propellant_1} & {rocket.engines.propellant_2}<br/>Layout : {rocket.engines.layout?rocket.engines.layout: "Nill"}</div></h4>
                            <h4>Landing legs : <div className="payloads-details">Number : {rocket.landing_legs.number}<br/>Material : {rocket.landing_legs.material}</div></h4>
                            <h4><u>Payloads</u><br/>
                                {rocket.payload_weights.map(function(payload , key){
                                    return(
                                        <h5 className="payloads-details" key={key}>({key+1}){payload.payload_id}
                                                Name : {payload.name} &#8594;
                                                Weight : {payload.kg}Kg
                                        </h5>
                                    )
                                })}
                            </h4>
                            <h4>Company : {rocket.company}<br/>Country : {rocket.country}</h4>
                            {rocket.active === true ? <h4>Status : Active</h4>:<h4>Status : Inactive</h4>}
                            <h4>Details : {rocket.description}</h4>

                        </div>
                    )
                })}
            </Layout>
        )
    }
}

export default allrockets