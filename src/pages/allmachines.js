import React, { Component }  from 'react';
import { Link } from 'gatsby'


class allmachines extends Component {

    render(){
        return(
            <div>
                <h1 className="major">All Machines</h1>
                <h2><Link to='/allrockets'>Rockets</Link></h2>
                <h2><Link to='/alldragons'>Dragons</Link></h2>
                <h2><Link to='/allcapsules'>Capsules</Link></h2>
                <h2><Link to='/allcores'>Cores</Link></h2>
                <h2><Link to='/allpayloads'>Payloads</Link></h2>
            </div>
        )
    }
}

export default allmachines