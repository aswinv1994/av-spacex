import React, { Component }  from 'react';
import axios from 'axios';
import moment from 'moment';
import ReactMomentCountDown from 'react-moment-countdown';
import {CircleArrow as ScrollUpButton} from "react-scroll-up-button";



import { Link } from 'gatsby'


import Layout from '../components/layout'

let headstyle = {
    textAlign: 'center',
    width: '100%',
    fontSize: '2rem'
};

let countdown = {
    textAlign:'center',
    width: '100%',
    fontSize: '5rem',
    paddingBottom: '5%'
}

// import pic01 from '../images/pic01.jpg'

class futureLaunches extends Component {

    constructor(props) {
        super(props);

        this.state = {
            futurelaunchData: []
        };
    }

    componentDidMount(){
        let futurelaunchAPI = 'https://api.spacexdata.com/v3/launches/upcoming?order=asc';
        axios.get(futurelaunchAPI).then(res=> {
            console.log(res.data);
            this.setState({futurelaunchData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major" style={headstyle}>Upcoming Launches | <Link to="/" style={{borderBottom: 'none'}}>Home</Link></h1>
                {this.state.futurelaunchData && this.state.futurelaunchData.map(function(launch, key) {
                    return(
                        <div key={key} className="image main">
                            {/*<div style={countdown}>*/}
                                {key === 0 ?
                                    <div style={countdown}>
                                        <h2 className='launch-head' style={{marginBottom: 0}}>Next Launch &#8594; {launch.mission_name} in</h2>
                                        <ReactMomentCountDown toDate={moment(launch.launch_date_local)} targetFormatMask='DD:HH:mm:ss' /><br/>
                                        <h4>Days : Hours : minutes : seconds</h4>
                                    </div> : ""}
                            {/*</div>*/}
                            <h2>{launch.mission_name}</h2>
                            <h4>Flight number : #{launch.flight_number}</h4>
                            <h4>Launching on {moment(launch.launch_date_utc).format('MMMM Do YYYY, h:mm:ss a')}<br/>Launch site : {launch.launch_site.site_name_long}</h4>
                            <h4>Rocket Name : {launch.rocket.rocket_name} [Type -- {launch.rocket.rocket_type}]</h4>
                            <h4><u>First stage :: cores</u><br/>
                                {launch.rocket.first_stage.cores.map(core =>{
                                    return(
                                        <li>{core.core_serial}</li>
                                    )
                                })}
                            </h4>
                            <h4><u>Second stage :: Payloads</u><br/>
                                {launch.rocket.second_stage.payloads.map(function(payload , key){
                                    return(
                                        <div key={key}>({key+1}){payload.payload_id}<br/>
                                            <div className="payloads-details">
                                                Customers : {payload.customers.map(cust =>{
                                                return(
                                                    <li>{cust}</li>
                                                )
                                            })}
                                                <br/>Nationality : {payload.nationality} <br/>Manufactured by {payload.manufacturer ? payload.manufacturer: "Unknown"}<br/>
                                                payload type : {payload.payload_type}
                                            </div>
                                        </div>
                                    )
                                })}
                            </h4>
                            {launch.details ? <h4>Launch Details : {launch.details}</h4>: <h4>Launch Details : nill</h4>}
                        </div>
                    )
                })}
                <ScrollUpButton
                    StopPosition={0}
                    ShowAtPosition={150}
                    EasingType='easeOutCubic'
                    AnimationDuration={500}
                    ContainerClassName='ScrollUpButton__Container'
                    TransitionClassName='ScrollUpButton__Toggled'
                    style={{backgroundColor: "transparent", border: "5px solid #4a4a4a"}}
                    ToggledStyle={{}}
                />
            </Layout>
        )
    }
}

export default futureLaunches