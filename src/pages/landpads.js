import React, { Component }  from 'react';
import axios from 'axios';
import { Link } from 'gatsby'

import Layout from '../components/layout'

class landpads extends Component {

    constructor(props) {
        super(props);

        this.state = {
            landpadData: []
        };
    }

    componentDidMount(){
        let landpadAPI = 'https://api.spacexdata.com/v3/landpads';
        axios.get(landpadAPI).then(res=> {
            console.log(res.data);
            this.setState({landpadData: res.data})
        });
    }


    render(){
        return(
            <Layout>
                <h1 className="major"><Link to="/" style={{borderBottom:'none'}}>&#8678;</Link> <span>Landing pads</span></h1>
                <h3>Active Landing pads </h3>
                {this.state.landpadData && this.state.landpadData.map(function(lpads, key) {
                    if(lpads.status==="active"){
                        return(
                            <div key={key} className="image main">
                                <h4>{lpads.full_name} ({lpads.id})</h4>
                                <h5>Location : {lpads.location.name}, {lpads.location.region}</h5>
                                <h5>{lpads.details}</h5>
                                <h5>Attempted Landings : {lpads.attempted_landings}<br/>Successfull Landings : {lpads.successful_landings}</h5>
                                <h5>Landing Type : {lpads.landing_type}</h5>
                                <h5><a href={lpads.wikipedia}>More Details</a></h5>
                            </div>
                        )
                    }
                })}
                <h3>Under Construction Landing pads</h3>
                {this.state.landpadData && this.state.landpadData.map(function(lpads, key) {
                    if(lpads.status==="under construction"){
                        return(
                            <div key={key} className="image main">
                                <h4>{lpads.full_name} ({lpads.id})</h4>
                                <h5>Location : {lpads.location.name}, {lpads.location.region}</h5>
                                <h5>{lpads.details}</h5>
                                <h5>Attempted Landings : {lpads.attempted_landings}<br/>Successfull Landings : {lpads.successful_landings}</h5>
                                <h5>Landing Type : {lpads.landing_type}</h5>
                                <h5><a href={lpads.wikipedia}>More Details</a></h5>
                            </div>
                        )
                    }
                })}

                <h3>Closed Landing pads</h3>
                {this.state.landpadData && this.state.landpadData.map(function(lpads, key) {
                    if(lpads.status==="retired"){
                        return(
                            <div key={key} className="image main">
                                <h4>{lpads.full_name} ({lpads.id})</h4>
                                <h5>Location : {lpads.location.name}, {lpads.location.region}</h5>
                                <h5>{lpads.details}</h5>
                                <h5>Attempted Landings : {lpads.attempted_landings}<br/>Successfull Landings : {lpads.successful_landings}</h5>
                                <h5>Landing Type : {lpads.landing_type}</h5>
                                <h5><a href={lpads.wikipedia}>More Details</a></h5>
                            </div>
                        )
                    }
                })}
            </Layout>
        )
    }
}

export default landpads