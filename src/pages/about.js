import React, { Component }  from 'react';
import axios from 'axios';
import { Link } from 'gatsby'



// import pic01 from '../images/pic01.jpg'

class About extends Component {

    constructor(props) {
        super(props);

        this.state = {
            infoData: []
        };
    }

    componentDidMount(){
        let infoAPI = 'https://api.spacexdata.com/v3/info';
        axios.get(infoAPI).then(res=> {
            console.log(res.data);
            this.setState({infoData: res.data})
        });
    }


    render(){
        return(
            <div>
                <h1 className="major">About Spacex</h1>
                <div className="image main">
                    <h3>Company info</h3>
                    <h4>Founded in <b>{this.state.infoData.founded}</b>, by {this.state.infoData.founder} <br/> with currently over <b>{this.state.infoData.employees}</b> employees,
                        <br/><Link to="/launchpads">{this.state.infoData.launch_sites} Launch sites/{this.state.infoData.test_sites} test site</Link>,<br/> <Link to="/landpads">5 Landing Pads</Link>
                        <br/><Link to="/allrockets">{this.state.infoData.vehicles} Vehicles</Link>
                    </h4>
                </div>
                <div className="image main">
                    <h3>Company leadership</h3>
                    <h4>CEO : {this.state.infoData.ceo}<br/>COO : {this.state.infoData.coo}<br/>CTO : {this.state.infoData.cto}<br/>CTO(Propulsion) : {this.state.infoData.cto_propulsion}</h4>
                </div>
            </div>
        )
    }
}

export default About