import React from 'react'
import PropTypes from 'prop-types'

const Footer = (props) => (
    <footer id="footer" style={props.timeout ? {display: 'none'} : {}}>
        <p className="copyright">&copy; <a href="https://av-portfolio.surge.sh">Aswin Varghese</a>.<br/> This project is <b>not affiliated with the SpaceX company</b> or any of its affiliates in any way.
            <br/>Created because I love space exploration and working with APIs.</p>
    </footer>
)

Footer.propTypes = {
    timeout: PropTypes.bool
}

export default Footer
