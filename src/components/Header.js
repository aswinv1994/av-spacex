import React from 'react'
import PropTypes from 'prop-types'

const Header = (props) => (
    <header id="header" style={props.timeout ? {display: 'none'} : {}}>
        <div className="logo">
            <img src="https://www.spacex.com/sites/spacex/files/spacex_logo_white.png" alt="SpaceX Logo"/>
        </div>
        <div className="content">
            <div className="inner">
                <h1>What is SpaceX?</h1>
                <p><a href="https://www.spacex.com/">SpaceX</a> designs, manufactures and launches advanced rockets and spacecraft.<br/> The company was founded in 2002 to revolutionize space technology, with the ultimate goal of enabling people to live on other planets.</p>
            </div>
        </div>
        <nav>
            <ul>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('intro')}}>About SpaceX</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('launches')}}>Launches</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('machines')}}>Machines</a></li>
                <li><a href="javascript:;" onClick={() => {props.onOpenArticle('launchpads')}}>Sites</a></li>
            </ul>
        </nav>
    </header>
)

Header.propTypes = {
    onOpenArticle: PropTypes.func,
    timeout: PropTypes.bool
}

export default Header
